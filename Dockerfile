FROM bdabo01/centos7:ansible
MAINTAINER bdabo

RUN yum update -y
RUN yum install -y python3 sshpass
RUN yum clean all

# Set files
WORKDIR /etc/ansible
RUN rm -rf hosts
COPY roles /etc/ansible
COPY hosts /etc/ansible
COPY main.yml /etc/ansible
COPY send_ssh_key.sh /etc/ansible
COPY hosts.txt /etc/ansible


# Exécution du script
RUN chmod +x send_ssh_key.sh; ./send_ssh_key.sh
#ENTRYPOINT [“send_ssh_key.sh”]

# Exécution du playbook
#RUN which ansible-playbook
RUN ansible-playbook main.yml -i /etc/ansible/hosts -v

