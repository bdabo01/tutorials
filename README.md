# Tutorials

In this project I'm gonna have my own wiki with my tutorials.

node {
    checkout scm
    def testImage = docker.build("test-image", ".") 

    testImage.inside {
        sh 'make test'
    }
}